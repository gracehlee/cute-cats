from django.urls import path
from cats.views import show_cat, cat_list, create_cat, edit_cat, about_cat, my_cat_list

urlpatterns = [
    path("<int:id>/", show_cat, name="show_cat"),
    path("", cat_list, name="cat_list"),
    path("<int:id>/edit/", edit_cat, name="edit_cat"),
    path("create/", create_cat, name="create_cat"),
    path("about/", about_cat, name="about_cat"),
    path("mine/", my_cat_list, name="my_cat_list"),
]
