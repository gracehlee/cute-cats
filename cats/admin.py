from django.contrib import admin
from cats.models import Cat, CatFact, Social

# Register your models here.
@admin.register(Cat)
class CatAdmin(admin.ModelAdmin):
    list_display = [
        "title",
        "id",
    ]

@admin.register(CatFact)
class CatFactsAdmin(admin.ModelAdmin):
    list_display = [
        "fact_number",
        "fact",
        "id",
    ]

@admin.register(Social)
class SocialAdmin(admin.ModelAdmin):
    list_display = [
        "site",
        "url",
        "id",
    ]
