from django.shortcuts import render, redirect
from cats.models import Cat
from cats.forms import CatForm
from django.contrib.auth.decorators import login_required

# Create your views here.
def about_cat(request):
    return render(request, "cats/about.html")

def show_cat(request, id):
    cat = Cat.objects.get(id=id)
    context = {
        "cat_object": cat
    }
    return render(request, "cats/detail.html", context)

def cat_list(request):
    cats = Cat.objects.all()
    context = {
        "cat_list": cats,
    }
    return render(request,"cats/list.html", context)

@login_required(login_url='login')
def my_cat_list(request):
    cats = Cat.objects.filter(author=request.user)
    context = {
        "cat_list": cats,
    }
    return render(request, "cats/list.html", context)

@login_required(login_url='login')
def create_cat(request):
    if request.method == "POST":
        form = CatForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("cat_list")
    else:
        form = CatForm()
    context = {
        "form": form,
    }
    return render(request, "cats/create.html", context)

@login_required(login_url='login')
def edit_cat(request, id):
    cat = Cat.objects.get(id=id)
    if request.method == "POST":
        form = CatForm(request.POST, instance=cat)
        if form.is_valid():
            form.save()
            return redirect("show_cat", id=id)
    else:
        form = CatForm(instance=cat)
    context = {
        "cat_object": cat,
        "cat_form": form,
    }
    return render(request, "cats/edit.html", context)
