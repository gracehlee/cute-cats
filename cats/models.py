from django.db import models
from django.conf import settings

# Create your models here.
class Cat(models.Model):
    title = models.TextField("Cat's Name", max_length=100)
    picture = models.URLField("Picture URL")
    rating = models.CharField("Rate ⭐", default="⭐⭐⭐⭐⭐", max_length=5)
    description = models.TextField()
    created_on = models.DateTimeField(auto_now_add=True)

    author = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="cats",
        on_delete=models.CASCADE,
        null=True,
    )

    def __str__(self):
        return self.title

class CatFact(models.Model):
    fact_number = models.PositiveSmallIntegerField()
    fact = models.TextField()
    cat = models.ForeignKey(
        "Cat",
        related_name="facts",
        on_delete=models.CASCADE,
        null=True,
    )

    class Meta:
        ordering = ["fact_number"]

    def cat_title(self):
        return self.cat.title

class Social(models.Model):
    site = models.CharField(max_length=50)
    url = models.URLField(max_length=150)
    cat = models.ForeignKey(
        "Cat",
        related_name="socials",
        on_delete=models.CASCADE,
    )
    class Meta:
        ordering = ["url"]
