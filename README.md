# Cute Cats

![Screenshot](./images/3.png)

An early Django-centric project showcasing some of my favorite 2000's era aesthetics.

Demo Video: https://youtu.be/cSZKaBTFBj4

This project explores the use of microservice architecture, SQLite3 database, and frontend business logic for visitors and users. Code was written primarily in Python, with the addition of many HTML and CSS design elements.

## Set Up

```
python -m venv .venv
.venv\Scripts\Activate.ps1
pip install -r requirements.txt
python manage.py runserver
```

## Screenshots
![Screenshot](./images/4.png)
- Detail view of individual cat

![Screenshot](./images/5.png)
- Submit cat form

![Screenshot](./images/6.png)
- About page
